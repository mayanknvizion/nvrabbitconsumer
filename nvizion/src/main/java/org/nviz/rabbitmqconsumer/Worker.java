package org.nviz.rabbitmqconsumer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.codehaus.jackson.map.ObjectMapper;
import org.nviz.rabbitmqconsumer.HMConstants.Connection.Authorization;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class Worker {
	Logger logger = Logger.getLogger(Worker.class.getName());
	String host;
	String queue_name;
	String workerName;
	Integer port;
	Integer max_total;
	Boolean test_on_borrow;
	Boolean test_on_return;
	Boolean test_while_idle;
	Boolean block_when_exhausted;
	JedisPoolConfig poolConfig;
	JedisPool jedispool;

	private String purgeUrl;

	private String authorizationKey;

	private String purgeHost;
	DateFormat dateFormat=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Date date= new Date();
	// private static final Logger logger =
	// LogManager.getLogger(CustomerOriginService.class);
	private static final int EDGECAST_PURGE_CONNECTION_TIMEOUT = (int) TimeUnit.SECONDS
			.toMillis(20L);

	public JedisPool getJedispool() {
		return jedispool;
	}

	public void setJedispool(JedisPool jedispool) {
		this.jedispool = jedispool;
	}

	public Integer getMax_total() {
		return max_total;
	}

	public void setMax_total(Integer max_total) {
		this.max_total = max_total;
	}

	public Boolean getTest_on_borrow() {
		return test_on_borrow;
	}

	public void setTest_on_borrow(Boolean test_on_borrow) {
		this.test_on_borrow = test_on_borrow;
	}

	public Boolean getTest_on_return() {
		return test_on_return;
	}

	public void setTest_on_return(Boolean test_on_return) {
		this.test_on_return = test_on_return;
	}

	public Boolean getTest_while_idle() {
		return test_while_idle;
	}

	public void setTest_while_idle(Boolean test_while_idle) {
		this.test_while_idle = test_while_idle;
	}

	public Boolean getBlock_when_exhausted() {
		return block_when_exhausted;
	}

	public void setBlock_when_exhausted(Boolean block_when_exhausted) {
		this.block_when_exhausted = block_when_exhausted;
	}

	public Worker(String workername) {
		this.workerName = workername;
	}

	public String getWorkerName() {
		return workerName;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getQueue_name() {
		return queue_name;
	}

	public void setQueue_name(String queue_name) {
		this.queue_name = queue_name;
	}

	public void setWorkerName(String workerName) {
		this.workerName = workerName;
	}

	public JedisPoolConfig getPoolConfig() {
		return poolConfig;
	}

	public void setPoolConfig(JedisPoolConfig poolConfig) {
		this.poolConfig = poolConfig;
	}

	public Worker() {
		super();
		// TODO Auto-generated constructor stub
	}

	// private static final String TASK_QUEUE_NAME = "task_queue";

	private static void doWork(String task) {
		for (char ch : task.toCharArray()) {
			if (ch == '.') {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException _ignored) {
					Thread.currentThread().interrupt();
				}
			}
		}
	}

	public static JedisPoolConfig buildPoolConfig(Properties prop) {

		final JedisPoolConfig poolConfig = new JedisPoolConfig();
		// poolConfig.setMaxTotal(Integer.valueOf(
		// prop.getProperty("REDIS.MAX_TOTAL")));
		// poolConfig.setTestOnBorrow(Boolean.valueOf(
		// prop.getProperty("REDIS.TEST_ON_BORROW")));
		// poolConfig.setTestOnReturn(Boolean.valueOf(
		// prop.getProperty("REDIS.TEST_ON_RETURN")));
		// poolConfig.setTestWhileIdle(Boolean.valueOf(
		// prop.getProperty("REDIS.TEST__WHILE_IDLE")));
		// poolConfig.setBlockWhenExhausted(Boolean.valueOf(
		// prop.getProperty("REDIS.BLOCK_WHEN_EXHAUSTED")));
		poolConfig.setMaxTotal(Integer.valueOf(prop.getProperty("REDIS.MAX_TOTAL")));
		return poolConfig;
	}

	public void startConsumer(final Worker w) {
		try {
			ConnectionFactory factory = new ConnectionFactory();
			/*
			 * for local setup comment factory.setUri() and uncomment the below
			 * lines
			 */
			factory.setUri(w.getHost());
			final Connection connection = factory.newConnection();
			final Channel channel = connection.createChannel();

			channel.queueDeclare(w.getQueue_name(), true, false, false, null);
			// System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
			logger.info(" [*] Waiting for messages. To exit press CTRL+C");
			int prefetchCount = 1;
			channel.basicQos(prefetchCount);
			// channel.basicQos(1);

			final Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag,
						Envelope envelope, AMQP.BasicProperties properties,
						byte[] body) throws IOException {
					String message = new String(body, "UTF-8");
					PurgeContent p= new PurgeContent();
					String tokenId = (message.split(",")[2].split(":")[1]).replace("}", "");
					p.setMediaPath((message.split(",")[1].split(":")[1]+":"+message.split(",")[1].split(":")[2]).replace("\"", ""));
					//p.setMediaType("14");
					System.out.println(" [x] Received '" + message + "'"
							+ " by consumer name:" + workerName);
					try {
						
						updateTokenStatus(w,
						tokenId,p);
				
					}
					catch(Exception e)
					{
						System.out.println(e);
					}
					finally {
						
						channel.basicAck(envelope.getDeliveryTag(), false);
						logger.info("token request "+tokenId+" done by "+workerName);
					}
				}
			};

			channel.basicConsume(w.getQueue_name(), false, consumer); // dequeue
			
			System.out.println("");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void updateTokenStatus(Worker w, String token , PurgeContent p) {
		try (Jedis jedis = w.getJedispool().getResource()) {
			System.out.println("updating  token status of "+token+" "+jedis.get(token));
			if(jedis.get(token).equalsIgnoreCase("Pending"))
			{
				jedis.set(token,hitEdgeCast(p));
			}
			System.out.println("Updated token status is "+jedis.get(token));
		}
	}

	/**
	 * This method is to get the response from Http url connection.
	 * 
	 * @param inputStream
	 * @return String
	 * @throws IOException
	 **/
	public String getResponse(InputStream inputStream) throws IOException {
		StringBuffer response = new StringBuffer();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				inputStream));
		String inputLine = null;
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		return response.toString();
	}

	/**
	 * This method is to create a HTTP Url Connection. This method accepts the
	 * url and type of request method.
	 * 
	 * @param url
	 * @param requestMethodType
	 * @return HttpURLConnection
	 * @throws IOException
	 **/

	public HttpURLConnection getConnection(String url, String requestMethodType) throws IOException {
		HttpURLConnection con = null;
		URL obj = new URL(url);
		con = (HttpURLConnection) obj.openConnection();
		if (null == con) {
			throw new IOException("Connection cannot be created for URL: " + url);
		}
		con.setRequestMethod(requestMethodType);
		con.setRequestProperty("Content-Type", HMConstants.Connection.ContentType.APPLICATION_JSON);
		con.setRequestProperty("Authorization", Authorization.CDN_CONNECTION_AUTHORIZATION_TOKEN);
		con.setRequestProperty("Accept", HMConstants.Connection.ContentType.APPLICATION_JSON);
		con.setRequestProperty("Host", Authorization.CDN_HOST);
		if (requestMethodType.equals(HMConstants.Cdn.REQUEST_POST)
				|| requestMethodType.equals(HMConstants.Cdn.REQUEST_PUT)) {
			con.setDoOutput(true);
		}
		con.setDoInput(true);
		return con;
	}

	public String getAuthorizationKey() {
		return authorizationKey;
	}

	/*public void setAuthorizationKey(String authorizationKey) {
		this.authorizationKey = authorizationKey;
	}*/

	public String getPurgeHost() {
		return purgeHost;
	}

	public void setPurgeHost(String purgeHost) {
		this.purgeHost = purgeHost;
	}

	public OutputStream writeBody(OutputStream outputStream, String requestBody)
			throws UnsupportedEncodingException, IOException {
		outputStream.write(requestBody
				.getBytes(HMConstants.Connection.Charset.UTF_8));
		outputStream.close();
		return outputStream;
	}

	public String getPurgeUrl() {
		return purgeUrl;
	}

	public void setPurgeUrl(String purgeUrl) {
		this.purgeUrl = purgeUrl;
	}

	public String hitEdgeCast(PurgeContent purgeContent) {
		logger.info("purge() started hitting EdgeCast");
		StringBuilder builder = new StringBuilder();
		BufferedReader in = null;
		String cdnReply="";
	try {
		
			while(!cdnReply.contains("Id"))
			{
				ObjectMapper mapper = new ObjectMapper();
				
				HttpURLConnection connection = getConnection(Authorization.CDN_URL, HMConstants.Cdn.REQUEST_PUT);
				connection.setConnectTimeout(EDGECAST_PURGE_CONNECTION_TIMEOUT);
				
				connection.setReadTimeout(EDGECAST_PURGE_CONNECTION_TIMEOUT);
				connection.connect();
					logger.info("URL connection created");
					//String requestBody = mapper.writeValueAsString(purgeContent);
					String requestBody="{\"MediaPath\":"+"\""+purgeContent.getMediaPath()+"\""+","+"\"MediaType\":"+purgeContent.getMediaType()+"}";
					logger.info("HTTP request for purging:  " + requestBody);
					OutputStream os = writeBody(connection.getOutputStream(), requestBody);
					int responseCode = connection.getResponseCode();
					logger.info("Purge Responsecode:" + responseCode);
					if (responseCode == 200) 
					{
						InputStream input = connection.getInputStream();
						in = new BufferedReader(new InputStreamReader(input));
						String inputLine;
						while ((inputLine = in.readLine()) != null) 
						{
							builder.append(inputLine);
						}
						cdnReply+=builder.toString();
						System.out.println("response at"+dateFormat.format(date));
					}
					else
					{
						
						if (in != null) 
						{
							in.close();
						}
						connection.disconnect();
					}
					/*else 
					
					{
							cdnReply+=getResponse(connection.getErrorStream());
							System.out.println(("Error Occured while submitting purge response reply from CDN is:" + cdnReply));
							System.out.println("response at"+dateFormat.format(date));
					}*/
			}
		
		} catch (Exception e) 
		{
			System.out.println("IO or Interuptted exception occured");
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
			return cdnReply;
		}

	

	public static void main(String[] args) {
		
		Properties prop = new Properties();
		InputStream input = null;
		try {
			

			String filename = "config.properties";
			input = Worker.class.getClassLoader().getResourceAsStream(filename);

			prop.load(input);
			final JedisPoolConfig poolConfig = buildPoolConfig(prop);
			JedisPool jedisPool = new JedisPool(poolConfig, prop.getProperty("REDIS.HOST"), Integer.parseInt(prop.getProperty("REDIS.PORT")));
			DateFormat dateFormat=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date= new Date();
			// get the property value and print it out
			System.out.println("started at"+dateFormat.format(date));
			for (int i = 0; i < Integer.parseInt(prop
					.getProperty("No_OF_WORKERS")); i++) {
				Worker w = new Worker("consumer" + i);
				w.setHost(prop.getProperty("RABBITMQ.HOST"));
				w.setPort(Integer.parseInt(prop.getProperty("RABBITMQ.PORTNO")));
				w.setQueue_name(prop.getProperty("TASK_QUEUE_NAME"));

				// w.setPoolConfig(poolConfig);
				w.setJedispool(jedisPool);
				w.startConsumer(w);
			}
			System.out.println("started " + prop.getProperty("No_OF_WORKERS")
					+ " consumers");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
