package org.nviz.rabbitmqconsumer;



import java.util.regex.Pattern;

public interface HMConstants {

	interface Connection {
		interface Charset {
			String UTF_8 = "UTF-8";
			Pattern CHARSET_PATTERN = Pattern.compile("(?i)\\bcharset=\\s*\"?([^\\s;\"]*)");
		}

		interface ContentType {
			String TEXT_HTML = "text/html";
			String TEXT_CSS = "text/css";
			String APPLICATION_JAVASCRIPT = "application/javascript";
			String TEXT_JAVASCRIPT = "text/javascript";
			String APPLICATION_JSON = "application/json";
		}

		interface Authorization {
			String CDN_CONNECTION_AUTHORIZATION_TOKEN = "TOK:0fadb926-aca5-4c18-9d65-1c4e496dd333";
			String CDN_URL="https://api.edgecast.com/v2/mcc/customers/DF9D/edge/purge";
			String CDN_HOST="api.edgecast.com";
		}
        
		interface Host {
			String PURGE_HOST = "api.edgecast.com";
		}
		
		interface URL {
			Pattern URL_REQUEST_PARAMS_PATTERN = Pattern.compile("[&;\\?][\\w\\W]*");
			Pattern URL_PATH_PATTERN = Pattern.compile("^(.*[\\/])");
			Pattern IMAGE_URL_PATTERN = Pattern.compile("url\\((.+?)\\)");
		}

		interface FileExtensions {
			String HTML_EXTN = ".html";
			Pattern RESOURCE_EXTNS = Pattern.compile(
					".*\\.(gif|GIF|jpg|JPG|png|PNG|ico|ICO|css|CSS|sit|SIT|eps|EPS|wmf|WMF|zip|ZIP|ppt|PPT|mpg|MPG|xls|XLS|gz|GZ|rpm|RPM|tgz|TGZ|mov|MOV|exe|EXE|jpeg|JPEG|bmp|BMP|js|JS|pdf|PDF)$");
			Pattern IMAGE_EXTNS = Pattern.compile(".*\\.(gif|GIF|jpg|JPG|png|PNG|jpeg|JPEG|bmp|BMP|ico|ICO|svg|SVG)$");
			Pattern FONT_EXTNS = Pattern.compile(".*\\.(woff|woff2|ttf|eot)$");
		}
	}

	interface Domain {
		String DOMAIN_NAME = "domainName";
		String DOMAIN_ID = "domainId";
		String SEGMENTS_PROPERTY_NAME = "segments";
		String ENTITY_TYPE = "Domain";
		String ENTITY_TYPE_PLURAL = "domains";
	}

	interface Segment {
		String SEGMENT_ID = "segmentId";
		String SEGMENT_NAME = "segmentName";
		String DEFAULT_SEGMENT_NAME = "default";
		int DEFAULT_SEGMENT_CRAWL_INTERVAL = 2592000;
		int DEFAULT_SEGMENT_PRIORITY = -1;
		int DEFAULT_SEGMENT_CRAWL = 1;
		String DEFAULT_SEGMENT_URL_PATTERN_RULE = "";
		String ENTITY_TYPE = "Segment";
		String RESOURCE_NAME = "Resource Name";
	}

	interface Transformation {
		String NAME = "name";
		String GROUP = "group";
		String ID = "id";
		String ATTRIBUTES = "attributes";
		String ATTRIBUTE_NAME = "attribute name";
		String SUPPORTED_CONTENT_TYPES = "supportedContentTypes";
		String ENTITY_TYPE = "Transformation";
	}

	interface Optimization {
		String NAME = "name";
		String GROUP = "group";
		String ID = "id";
		String VALUE = "value";
		String CONFIG_PROPERTY_NAME = "configPropertyName";
		String ENTITY_NAME = "Optimization";
	}

	interface HMRequest {
		String EVENT = "event";
		String IS_API = "isApi";
		String HOST_NAME = "hostName";
		String REQUEST_ID = "requestId";
		String STATUS = "status";
		String ACTION = "action";
		String ENTITY_TYPE = "Request";
	}

	interface Cdn {
		String REQUEST_GET = "GET";
		String REQUEST_POST = "POST";
		String REQUEST_PUT = "PUT";
		int MEDIA_TYPE_ID = 14;
	}

	interface Site {
		String NAME = "siteName";
		String ID = "id";
		String GROUP = "group";
		String VALUE = "value";
		String CONFIG_PROPERTY_NAME = "configPropertyName";
		String ENTITY_TYPE = "Site";
	}
	interface File
	{
		String APACHECONFIG="ApacheConfig";
		String TEMPLATECONFIG="TemplateConfig";
		String ROUTINGCONFIGSYSTEMPROPERTY="N7RoutingConfiguration";
	}
	int TRUE = 0;
	int FALSE = 1;
	int SUCCESS = 0;
	int FAILURE = 1;
	Pattern URL_PATH_PATTERN = Pattern.compile("^(.*[\\/])");
	String SPACE = " ";
	String FORWARD_SLASH = "/";
	String EMPTY_STRING = "";
	String QUESTION_MARK = "?";
}
