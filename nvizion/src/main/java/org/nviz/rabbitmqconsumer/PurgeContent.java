package org.nviz.rabbitmqconsumer;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PurgeContent {

	@JsonProperty("MediaPath")
	private String MediaPath;
	
	@JsonProperty("MediaType")
	private final int MediaType=14;

	public String getMediaPath() {
		return MediaPath;
	}

	public void setMediaPath(String mediaPath) {
		MediaPath = mediaPath;
	}

	public int getMediaType() {
		return MediaType;
	}

	

	

	
}
